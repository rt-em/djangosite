# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from pytils.translit import slugify
from django.contrib.auth import get_user_model


class SiteCategory(MPTTModel):
    name = models.CharField(max_length=200, verbose_name='Категория', unique=True)
    slug = models.SlugField(max_length=200, verbose_name='URL', default='' )
    icon = models.ImageField(upload_to='sitecatalog/icon/', verbose_name='Иконка категории', blank=True, null=True, )
    background = models.ImageField(upload_to='sitecatalog/background/', verbose_name='Фон категории', blank=True, null=True, )
    parent = TreeForeignKey(
        'self', on_delete=models.CASCADE, verbose_name='Родитель', null=True, blank=True, related_name='children'
    )

    class Meta:
        verbose_name = u"Категория каталога"
        verbose_name_plural = u"Категории каталога"

    class MPTTMeta:
        order_insertion_by = ['name']

    def __unicode__(self):
        return self.name

    def get_slug_list(self):
        try:
            ancestors = self.get_ancestors(include_self=True)
        except:
            ancestors = []
        else:
            ancestors = [ i.slug for i in ancestors]
            slugs = []
        for i in range(len(ancestors)):
            slugs.append('/'.join(ancestors[:i+1]))
        return slugs

    def get_full_slug(self):
        try:
            an = self.get_ancestors(include_self=True)
        except:
            an = []
        else:
            an = [i.slug for i in an]
            fslug = '/'
            fslug=fslug.join(an)
        return fslug

class Sites(models.Model):
    category = TreeForeignKey('SiteCategory', verbose_name='Категория')
    name = models.CharField(max_length=250, verbose_name='Название сайта', unique=True)
    slug = models.SlugField(max_length=250, verbose_name='slug', default='', blank=True, null=True, )
    url = models.URLField(verbose_name="Ссылка на сайт", unique=True)
    city = models.ForeignKey('Cities', verbose_name='Город', blank=True, null=True, )
    address = models.CharField(max_length=250, verbose_name='Адрес', unique=True, blank=True, null=True, )
    description = models.TextField(max_length=1000, verbose_name='Описание')
    picture = models.ImageField(upload_to='sitecatalog/background/', verbose_name='Скриншот главной страницы', blank=True, null=True, )
    published = models.BooleanField(default=False, verbose_name='Опубликовано', )
    nofollow = models.BooleanField(default=False, verbose_name='nofollow?')
    payed = models.DateField(verbose_name='Оплачено до', blank=True, null=True, )
    user = models.ForeignKey(get_user_model(), verbose_name='Пользователь', editable=False, null=True, blank=True, )
    creation = models.DateField(auto_now_add=True, verbose_name='Дата создания',  null=True, blank=True, )



    class Meta:
        verbose_name = u"Сайт"
        verbose_name_plural = u"Сайты"

    def save(self, *args, **kwargs):
        #this line below give to the instance slug field a slug name
        self.slug = slugify(self.name)
        #this line below save every fields of the model instance
        super(Sites, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.url

class Cities(models.Model):
    name = models.CharField(max_length=250, verbose_name='Город')
    country = models.ForeignKey('Country', default=1)

    class Meta:
        verbose_name = u"Город"
        verbose_name_plural = u"Города"

    def __unicode__(self):
        return self.name

class Country(models.Model):
    name = models.CharField(max_length=250, verbose_name='Страна')

    class Meta:
        verbose_name = u"Страна"
        verbose_name_plural = u"Страны"

    def __unicode__(self):
        return self.name