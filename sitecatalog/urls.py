from django.conf.urls import *
from sitecatalog.views import *
#from sitecatalog.sitemaps import *
from django.contrib.sitemaps.views import sitemap

urlpatterns = [
    url(r'^$', catalog),
    url(r'^(?P<hierarchy>.+)/$', show_category, name='category'),
]