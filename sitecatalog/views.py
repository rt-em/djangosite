# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from sitecatalog.models import SiteCategory, Sites
from django.shortcuts import render, get_object_or_404
from datetime import *


def catalog(request):
    cat = SiteCategory.objects.all()
    sites = Sites.objects.all()
    today = datetime.now()
    return render(request, '/sitecatalog/catalog_main.html', {'cat':cat, 'sites':sites, 'today': today})

def show_category(request, hierarchy=None):
    category_slug = hierarchy.split('/')
    parent = None
    root = SiteCategory.objects.all()

    for slug in category_slug[:-1]:
        parent = root.get(parent=parent, slug = slug)

    instance = SiteCategory.objects.get(parent=parent,slug=category_slug[-1])
    x=[]
    for j in instance.get_descendants(include_self=True):
        x.append(j)
        sites = Sites.objects.filter(category__in=x)

    return render(request, '/sitecatalog/catalog.html', {'instance':instance, 'sites':sites, })

# Create your views here.
