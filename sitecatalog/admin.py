# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from mptt.admin import DraggableMPTTAdmin
from sitecatalog.models import SiteCategory, Sites, Cities, Country


class CustomMPTTModelAdmin(DraggableMPTTAdmin):
    mptt_level_indent = 20
    prepopulated_fields = {"slug": ("name",)}

class SitesAdmin(admin.ModelAdmin):
    list_display = ('url', 'name', 'category', 'city', 'published', 'nofollow', 'payed', 'user', 'creation')
    list_filter = ('category', 'city', 'published', 'nofollow', 'user')
    exclude = ('slug',)
    search_fields = ('url', 'name', 'category', 'user')
    #prepopulated_fields = {"slug": ("name",) }

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'user', None) is None:
            obj.user = request.user
        obj.save()

class CitiesAdmin(admin.ModelAdmin):
    list_display = ('name', 'country', )
    list_filter = ('country',)
    search_fields = ('name', 'country', )

class CountryAdmin(admin.ModelAdmin):
    list_display = ('name', )
    search_fields = ('name', )

admin.site.register(
    SiteCategory,
    DraggableMPTTAdmin,
    list_display=(
        'tree_actions',
        'indented_title',
    ),
    list_display_links=(
        'indented_title',
    ),
    search_fields = ('name', 'slug',),
    prepopulated_fields = {"slug": ("name",)}
)
admin.site.register(Sites, SitesAdmin)
admin.site.register(Cities, CitiesAdmin)
admin.site.register(Country, CountryAdmin)