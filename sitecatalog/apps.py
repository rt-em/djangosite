# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class SitecatalogConfig(AppConfig):
    name = 'sitecatalog'
    verbose_name = 'Каталог сайтов'

