# -*- coding: utf-8 -*-
from __future__ import absolute_import, print_function, unicode_literals

from cms.sitemaps import CMSSitemap
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.static import serve
from des import urls as des_urls
#from machina.app import board
from cms import views as cms_views
from django.urls import LocaleRegexURLResolver, get_resolver
def _patch_lang_pfx():
    for url_pattern in get_resolver(None).url_patterns:
        if isinstance(url_pattern, LocaleRegexURLResolver):
            return url_pattern.prefix_default_language
    return False
# Swap out the faulty method from cms.views
cms_views.is_language_prefix_patterns_used = _patch_lang_pfx


admin.autodiscover()

urlpatterns = [
    url(r'^django-des/', include(des_urls)),
    #url(r'^catalog/', include('sitecatalog.urls')),
    #url(r'^accounts/', include('allauth.urls')),
    #url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    #url(r'^forum/', include(board.urls)),
    url(r'^', include('djangocms_forms.urls')),
    url(r'^taggit_autosuggest/', include('taggit_autosuggest.urls')),
    url(r'^robots\.txt', include('robots.urls')),
    url(r'^sitemap\.xml$', sitemap,
        {'sitemaps': {'cmspages': CMSSitemap}}),
]

urlpatterns += i18n_patterns(
    url(r'^admin/', include(admin.site.urls)),  # NOQA
    url(r'^', include('cms.urls')),
    prefix_default_language=False,
)

# This is only needed when using runserver.
if settings.DEBUG:
    urlpatterns = [
        url(r'^media/(?P<path>.*)$', serve,
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
        ] + staticfiles_urlpatterns() + urlpatterns
