# -*- coding: UTF-8 -*-
import os  # isort:skip
from machina import get_apps as get_machina_apps
from machina import MACHINA_MAIN_TEMPLATE_DIR
from machina import MACHINA_MAIN_STATIC_DIR
gettext = lambda s: s
DATA_DIR = os.path.dirname(os.path.dirname(__file__))

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'very_secret_key'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['*']


# Application definition

ROOT_URLCONF = 'djangosite.urls'

# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'ru'

TIME_ZONE = 'Europe/Moscow'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

STATIC_URL = '/static/'
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(DATA_DIR, 'media')
STATIC_ROOT = os.path.join(DATA_DIR, 'static')

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'djangosite', 'static'),
    #MACHINA_MAIN_STATIC_DIR,
)

SITE_ID = 1


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'djangosite', 'templates'),
                # os.path.join(BASE_DIR, 'djangosite', 'templates', 'machina'),
                 ],
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.i18n',
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.template.context_processors.media',
                'django.template.context_processors.csrf',
                'django.template.context_processors.tz',
                'sekizai.context_processors.sekizai',
                'django.template.context_processors.static',
                'cms.context_processors.cms_settings',
                #'machina.core.context_processors.metadata',
            ],
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
                'django.template.loaders.eggs.Loader'
            ],
        },
    },
]


MIDDLEWARE = (
    'cms.middleware.utils.ApphookReloadMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware',
#    'machina.apps.forum_permission.middleware.ForumPermissionMiddleware',
)

INSTALLED_APPS = [
    'djangocms_admin_style',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.admin',
    'django.contrib.sites',
    'django.contrib.sitemaps',
    'django.contrib.staticfiles',
    'django.contrib.messages',
    'cmsplugin_cascade',
    'cmsplugin_cascade.clipboard',  # optional
    'cmsplugin_cascade.extra_fields',  # optional
    'cmsplugin_cascade.sharable',  # optional
    'cms',
    'menus',
    'sekizai',
    'treebeard',
    'djangocms_text_ckeditor',
    'filer',
    'easy_thumbnails',
    #'djangocms_column',
    'djangocms_file',
    'djangocms_link',
    'djangocms_picture',
    'djangocms_style',
    'djangocms_snippet',
    'djangocms_googlemap',
    'djangocms_video',
    'djangosite',
    'robots',
    'aldryn_background_image',
    'djangocms_unitegallery',
    'djangocms_forms',
    'des',
    'cmsplugin_yandexmap',
    'djangocms_icon',
    'solo',
    'djangocms_custommenu',
    'aldryn_apphooks_config',
    'cmsplugin_filer_image',
    'parler',
    'taggit',
    'taggit_autosuggest',
    'meta',
    'sortedm2m',
    'djangocms_blog',
    'widget_tweaks',
    #'allauth',
    #'allauth.account',
    #'allauth.socialaccount',
    #'allauth.socialaccount.providers.odnoklassniki',
    #'allauth.socialaccount.providers.vk',
    #'allauth.socialaccount.providers.google',
    #'allauth.socialaccount.providers.facebook',
    #'crispy_forms',
    # Machina related apps:
    #'mptt',
    #'haystack',
    #'captcha',
    #'ckeditor',
    #'ckeditor_uploader',
    #'sitecatalog',
]    \
                 #+ get_machina_apps()


LANGUAGES = (
    ## Customize this
    ('ru', gettext('ru')),
)

CMS_LANGUAGES = {
    ## Customize this
    'default': {
        'public': True,
        'hide_untranslated': False,
        'redirect_on_fallback': True,
    },
    1: [
        {
            'public': True,
            'code': 'ru',
            'hide_untranslated': False,
            'name': gettext('ru'),
            'redirect_on_fallback': True,
        },
    ],
}

CMS_TEMPLATES = (
    ## Customize this
    ('paper.html', 'Bootswatch Paper'),
    ('flatly.html', 'Bootswatch Flatly'),
    ('united.html', 'Bootswatch United'),
    ('cosmo.html', 'Bootswatch Cosmo'),
    ('flyper.html', 'Flyper'),
    ('blank.html', 'Blank'),
    ('/startbootstrap-creative/index.html', 'SB-Creative(LP)'),
    ('/startbootstrap-creative/simple.html', 'SB-Creative(simple)'),
    ('/tyutrin/regular.html', 'Tyutrin_simple'),
    ('/tyutrin/main.html', 'Tyutrin_main'),

)

CMS_PERMISSION = True

DATABASES = {
    'default':
        {'ENGINE': 'django.db.backends.postgresql_psycopg2',
         'NAME': 'project_name',
         'HOST': 'localhost',
         'USER': 'project_name',
         'PASSWORD': 'db_pass',
         'PORT': '5432'}
}

MIGRATION_MODULES = {
    
}


THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters'
)

META_SITE_PROTOCOL = 'http'
META_USE_SITES = True

CMSPLUGIN_CASCADE_PLUGINS = ['cmsplugin_cascade.bootstrap3']

CMSPLUGIN_CASCADE = {
    'alien_plugins': ('TextPlugin', 'FilerImagePlugin', 'SnippetPlugin', 'FormPlugin',
    'FilerVideoPlugin', 'GalleryPlugin', 'GoogleMapPlugin',
    'StylePlugin', 'OEmbedVideoPlugin', 'BackgroundImagePlugin', 'FilePlugin',
    'LinkPlugin', 'FolderPlugin', 'PicturePugin', 'YandexMapPlugin', 'VideoPlayerPlugin',
    'IconPlugin', 'CustomMenuPlugin', 'BreadcrumbsPlugin', 'AutoMenuPlugin',
    'BlogPlugin', 'BlogLatestEntriesPlugin', 'BlogLatestEntriesPluginCached',
    'BlogAuthorPostsPlugin', 'BlogTagsPlugin', 'BlogCategoryPlugin', 'BlogArchivePlugin'),
}

ROBOTS_USE_HOST = True
ROBOTS_USE_SCHEME_IN_HOST = True
ROBOTS_CACHE_TIMEOUT = 60*60*24

DJANGOCMS_UNITEGALLERY_CONFIG = {
    'THUMBNAIL_ENABLED': True,
    'THUMBNAIL_MAX_WIDTH': 350,
    'THUMBNAIL_MAX_HEIGHT': 350,
    'THUMBNAIL_PRESERVE_RATIO': False,
}

DJANGOCMS_FORMS_WIDGET_CSS_CLASSES = {'textarea': ('form-control',),
                                      'text':('form-control',),
                                      'email':('form-control',),
                                      'number':('form-control',),
                                      'url':('form-control',),
                                      'date':('form-control',),
                                      'time':('form-control',),
                                      'password':('form-control',),
                                      'phone':('form-control',),
                                      'select':('form-control',),
                                      'checkbox':('checkbox',),
                                      }
DJANGOCMS_FORMS_DEFAULT_TEMPLATE = 'djangocms_forms/form_template/default.html'

DJANGOCMS_FORMS_TEMPLATES = (
    ('djangocms_forms/form_template/default.html', ('Default')),
    ('djangocms_forms/form_template/white_text.html', ('white_text')),
)

EMAIL_BACKEND = 'des.backends.ConfiguredEmailBackend'

DJANGOCMS_ICON_SETS = [
    ('fontawesome5', 'fa', 'Font Awesome'),
]


#DJANGOCMS_CUSTOMMENU_TEMPLATES = [
#    ('feature', _('Featured Version')),
#]

CMS_PLACEHOLDER_CONF = {
    'menu': {
        'plugins': ['LinkPlugin',],
        'name':gettext(u"Меню"),
    },
    'background': {
        'plugins': ['BackgroundImagePlugin',],
        'name':gettext(u"Фон первого экрана"),
    },
    'header_content': {
        'plugins': ['TextPlugin', 'YandexMapPlugin', 'FormPlugin', 'IconPlugin', 'PicturePlugin', ],
        'name':gettext(u"Контент первого экрана"),
    },
    'brand': {
        'plugins': ['TextPlugin', 'IconPlugin', 'PicturePlugin', ],
        'name':gettext(u"Брэнд/Лого"),
    },
    'header_snippet': {
        'plugins': ['SnippetPlugin',],
        'name':gettext(u"Скрипты верхней части страницы"),
    },
    'footer_snippet': {
        'plugins': ['SnippetPlugin', ],
        'name':gettext(u"Скрипты нижней части страницы"),
    },
}

DJANGOCMS_SNIPPET_THEME = 'github'
DJANGOCMS_SNIPPET_MODE = 'html'

PARLER_LANGUAGES = {
    1: (
        {'code': 'ru',},
    ),
    'default': {
        'fallbacks': ['ru', ],
    }
}

DJANGOCMS_VIDEO_ALLOWED_EXTENSIONS = ['mp4', 'webm', 'ogv']

#CACHES = {
#    'default': {
#        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
#    },
#    'machina_attachments': {
#        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
#        'LOCATION': '/tmp',
#    },
#}

#HAYSTACK_CONNECTIONS = {
#    'default': {
#        'ENGINE': 'haystack.backends.simple_backend.SimpleEngine',
#    },
#}

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
#    'allauth.account.auth_backends.AuthenticationBackend',
)

#CRISPY_TEMPLATE_PACK = 'bootstrap3'

LOGIN_REDIRECT_URL = '/'

BLOG_PLUGIN_TEMPLATE_FOLDERS = (('plugins', ('Default template')),
                                ('main_page', ('Mainpage template')),
                                ('news 2-3', ('news 2-3')),
                                ('news 4-8', ('news 4-8')),
                                ('news_list', ('news list')),
                               )

#RECAPTCHA_PUBLIC_KEY = 'get from google'
#RECAPTCHA_PRIVATE_KEY = 'get from google'

#RECAPTCHA_USE_SSL = True     # Defaults to False

DJANGOCMS_FORMS_RECAPTCHA_PUBLIC_KEY = 'get from google'
DJANGOCMS_FORMS_RECAPTCHA_SECRET_KEY = 'get from google'

#ACCOUNT_SIGNUP_FORM_CLASS = 'signup2.forms.AllauthSignupForm'

#CKEDITOR_BASEPATH = "/static/ckeditor/ckeditor/"
#CKEDITOR_UPLOAD_PATH = "uploads/"
#CKEDITOR_FILENAME_GENERATOR = 'utils.get_filename'
#CKEDITOR_IMAGE_BACKEND = "pillow"


#MACHINA_MARKUP_LANGUAGE = None
#MACHINA_MARKUP_WIDGET = 'ckeditor.widgets.CKEditorWidget'

